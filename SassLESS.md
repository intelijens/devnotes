Developer-Notes
===============

Just a small list of things I'd like to remember regarding Sass and LESS


- Sass is in Ruby and LESS is in JavaScript


- 
###Sass(not SASS)Syntacially Awesome Stylesheets

-Sassy CSS(.scss) is the default file extention
-A second syntax(.sass)

#####commenting
single line Sass comments - //Comments, wont compile into the css file
but multiline CSS comments /*Comments will compile into css file*/

#####importing
@import "buttons" add imported css at complile time vs on client side as when importing regular css


#####nesting

.content{
  border: 1px solid black;
  padding: 20px
  
  h2{
    font-size: 3em;
    margin: 20px 0px;
  }
  p{
    font-size: 1em;
    margin: 15px 4px;
  }
}

vs

.content{
  border: 1px solid black;
  padding: 20px
}
.content h2{
    font-size: 3em;
    margin: 20px 0px;
}
.content p{
    font-size: 1em;
    margin: 15px 4px;
}



&


sass --watch style.scss:style.css











####Start by installing Sass via command line

$ `sudo gem install sass`




