Markdown-Notes
===============

Just a small list of things I'd like to remember regarding Markdown(.md) Syntax


# This is an H1
## This is an H2
###### This is an H6

*Italic characters*

**bold characters**

1. Step 1
2. Step 2
3. Step 3
    * Item 3a
    * Item 3b
    * Item 3c


> Introducing my quote

Use the backtick to refer to a `function()`.
  
There is a literal ``backtick (`)`` here.
  

 
```
use 3 backtick quote marks before and after the block
``` 

	Indent every line of the block by at least 4 spaces or 1 tab.


[This link](http://example.net/) has no title attribute.


