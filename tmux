brew install tmux

tmux -V

Fire up a terminal and cd into the root of your application

tmux

CTRL+B % //split horizontal

CTRL+B " //split vertical

CTRL+B <UP ARROW> //navigate thru panes

CTRL+B  z //toggle full screen

CTRL+B c //new pane free terminal

CTRL+B <window number> //hops back into that window session

CTRL+B d //detach window, you’ll be back at your no-status-bar, regular, old terminal

tmux ls //you will see a list of sessions:

tmux attach -t 0 

tmux kill-session -t 0


