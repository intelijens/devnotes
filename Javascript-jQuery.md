Developer-Notes
===============

Just a small list of things I'd like to remember regarding JS and jQuery

-Variables
var firstName = "Cookie";

document.write(firstName);

var name = prompt('What is your name?', '');

document.write('<p>Welcome ' + name + '</p>');



-Arrays

var days = ['Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat', 'Sun'];
var days = new Array('Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat', 'Sun');
//same - This method is valid, but the method used in this book (called an array literal) is preferred by the pros because it requires less typing, less code, and is considered more “elegant.”


var playList = [];
//blank array

 var prefs = [1, 223, 'www.oreilly.com', false];
 //boolean okay
 
 var authors = [ 'Ernest Hemingway',
                    'Charlotte Bronte',
                    'Dante Alighieri',
                    'Emily Dickinson'
                  ];
  //multiline
  
alert(days[0]);
days[days.length-1]


var properties = ['red', '14px', 'Arial'];
properties[3] = 'bold';
properties[properties.length] = 'bold';
// add to the end

properties.push('bold');
//same - add to the end

####properties.push('bold', 'italic', 'underlined');
// add mutliple items to the end of array



var properties = ['red', '14px', 'Arial'];
####properties.unshift('bold');
// add to the beginning of array

####properties.unshift('bold', 'italic', 'underlined');
// add mutliple items to the beginning of array


<table>
<tr>
  <td>Method</td>
  <td>Original array</td>
  <td>Example code</td>
  <td>Resulting array</td>
  <td>Explanation</td>
</tr>
<tr>
  <td>pop()</td>
  <td>var p =
[0,1,2,3]</td>
  <td>p.pop()</td>
  <td>[0,1,2]</td>
  <td>Removes the last item from the array.</td>
</tr>
<tr>
  <td>shift()</td>
  <td>var p = [0,1,2,3]</td>
  <td>p.shift()</td>
  <td>[1,2,3] </td>
  <td>Removes the first item from the array.</td>
</tr>
</table>


//As with push() and unshift(), pop() and shift() return a value once they’ve completed their tasks of removing an item from an array. In fact, they return the value that they just removed. So, for example, this code removes a value and stores it in the variable removedItem:
     var p = [0,1,2,3];
     var removedItem = p.pop();
The value of removedItem after this code runs is 3 and the array p now contains [0,1,2].



#METHODS
.alert() //window - alert box //same as window.alert()
.prompt() //window - allert box with input //same as window.prompt()
.write() //document
.shift() //array - remove first item
.unshift('something') //-array - add to first item
.pop() //array - remove last item
.push('something') //array - add to last time


.toFixed(2); // decimal - $54.00


##COMMANDS
parseInt(someVariable, 10) //command takes a value and tries to convert it to an integer,
isNaN(someVariable) //NaN - Not a Number - command that checks to see if something is a number

calculateTotal(2, 16.95);



####Conditional statements are also called “if/then” statements


WHILE LOOP
FOR LOOP
DO/WHILE LOOP   



##FUNCTIONS

####DATE
function printToday(){
 var today = new Date();
 document.write(today.toDateString());
}



function functionName(parameter1, parameter2) { // the JavaScript you want to run
return value;
}


//////////////////////////////////////////////////////////
#jQuery

<script>
$(document).ready(function(){
//your stuff
});
</script>

####Selectors

$('selector')
$('#someId')
$('.someClass')
$('a')


$('#navBar a') // decendent

$('body > p') // child

$('h2 + div') // adjacent

$('img[alt]') // attribute

####Filters

$('tr:even') // :even and :odd

$('p:first') // :first and :last

$('a:not(.navButton)') // :not()

$('li:has(a)') // :has()

$('a:contains(Click Me!)') // :contains()

$('div:hidden').show(); :hidden and :visible


####Chaining Functions

$('#popUp').width(300).height(300).text('Hi!').fadeIn(1000);

 $('#popUp').width(300)
       .height(300)
       .text('Message')
       .fadeIn(1000);


####Builtin Functions
.html()
.text()
.append() and .prepend()
.after() and .before()
.remove()
.replaceWith()
.clone()
.val()
.hover()

.hide() and .show()
.toggle()

.fadeOut() and .fadeIn()
.fadeToggle()
.fadeTo();

.slideDown()
.slideUp().
.slideToggle();

.animate();

easing - linear, easeInBounce

.next()

//CLASSES
.addClass()
.removeClass()
.toggleClass()

//CSS
var bgColor = $('#main').css('background-color'); //retrieve/read
---
$('body').css('font-size', '200%'); //set

  $('#highlightedDiv').css({
      'background-color' : '#FF0000',
      'border' : '2px solid #FE0037'
});  //set multiple css properties
 

//ATTRIBUTES
 var imageFile = $('#banner img').attr('src'); //retrieve/read

$('#banner img').attr('src','images/newImage.png'); // set

 $('body').removeAttr('bgColor'); //remove
 
 
 //EACH
 
 $('selector').each();
 
 
 //Anonymous Functions
 
 
 //this and $(this)
 
 
 
 
 ##EVENTS
 
 //Mouse Events
 .click
 .dblclick
 .mousedown and .mouseup
 .mouseover and .mouseout
 .mousemove
 
 //Document-Window Events
 .load
 .resize
 .scroll
 .unload //When you click a link to go to another page, close a browser tab, or close a browser window, a web browser fires an unload event.
 
 //Form Events
 .submit
 .reset
 .change
 .focus
 .blur //The blur event is the opposite of focus. It’s triggered when you exit a cur- rently focused field, by either tabbing or clicking outside the field.
 
 //Keyboard
 .keypress
 .keydown and .keyup
 
 
 ##ANIMATE
 
 $('#login').animate(
	    {
	      left: '650px',
	      opacity: .5,
	      fontSize: '24px'
	}, 1500 );
	
	
 
 ##IMPROVING IMAGES
 
####Swap image source
<img src="images/image.jpg" width="100" height="100" id="photo">
$('#photo').attr('src','images/newImage.jpg');
______________________________________________

//New image variable, assign source
//New variable selects div ID photo, assign attributes to varialbe
var newPhoto = new Image();
newPhoto.src = 'images/newImage.jpg';
var photo = $('#photo');
photo.attr('src',newPhoto.src);
photo.attr('width',newPhoto.width);
photo.attr('height',newPhoto.height);
_____________________________________

//New image variable, assign source
//Select div ID photo, assign attributes directly
var newPhoto = new Image();
newPhoto.src = 'images/newImage.jpg';

$('#photo').attr({
  src: newPhoto.src,
  width: newPhoto.width,
  height: newPhoto.height
});

####Preload images
var newPhoto = new Image();
newPhoto.src = 'images/newImage.jpg';
_____________________________________


var preloadImages = ['images/roll.png',
                     'images/flower.png',
                     'images/cat.jpg'];

var imgs = [];
for (var i=0; i<preloadImages.length;i++) {
  imgs[i] = new Image();
  imgs[i].src = preloadImages[i];
}

####Rollover Images







