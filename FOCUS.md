Developer-Notes
===============

Just a small list of things I'd like to remember regarding staying focused and motivated.

##CUT OUT THE DISTRACTIONS
-TV
-Facebook/Instagram
-Email

##TRY TO BE ORGANIZED
-But don't use cleaning as a DISTRACTION!!!
-Clean work area helps

##BE RESTED
-But don't use sleeping as a DISTRACTION!!!
-Working when you're too tired is useless, you'll just stare at nothing and get litte accomplished

##NO SLOUCHING
-sit up straight and preferablly at your desk

##EFFICENCY
-Try to memorize the hot keys for actions made with your tools

##KEEP FAST VIBE
-Upbeat music reccommended

###Can you be better?  Raise to the challenge?  Top Chef, Porject Runway, Ink Masters
