Developer-Notes
===============

Just a small list of things I'd like to remember regarding Angular.js


###DIRECTIVES
ng-app // tag knows angular js app

ng-model // a name for input field //directive binds the value of HTML controls (input, select, textarea) to application data.

ng-init // initiate input to a model name // ng-init is not very common. You will learn a better way to initialize data in the chapter about controllers.

ng-bind // binds it to the tag

ng-click //

use data.ng to be valid html5

####Repeating html elements

ng-repeat directive clones HTML elements once for each item in a collection (in an array).  

<div ng-app="" ng-init="names=['Jani','Hege','Kai']">
  <ul>
    <li ng-repeat="x in names">
      {{ x }}
    </li>
  </ul>
</div>

###EXPRESSIONS

<div ng-app="">
  <p>My first expression: {{ 5 + 5 }}</p>
</div>



###CONTROLLERS

 <div ng-app="" ng-controller="personController">

First Name: <input type="text" ng-model="firstName"><br>
Last Name: <input type="text" ng-model="lastName"><br>
<br>
Full Name: {{firstName + " " + lastName}}

</div>

<script>
function personController($scope) {
    $scope.firstName = "John";
    $scope.lastName = "Doe";
}
</script> 

AngularJS will invoke personController with a $scope object.

In AngularJS, $scope is the application object (the owner of application variables and functions).




