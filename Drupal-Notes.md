Drupal-Notes
===============

Just a small list of things I'd like to remember regarding Drupal 



####Download and add files to htdocs via ftp or 
-   Drush is a command line tool to maintain and administer Drupal sites. It offers the most convenient way of downloading Drupal by using a single command: pm-download (or its alias dl):
drush dl drupal

####Create the db 
-   - via phpMyAdmin, 
-     commandline or 
-     PostgreSQL
-       Create a database user
This step is only necessary if you don't already have a user setup (e.g. by your host) or you want to create new user for use with Drupal only.
 `createuser --pwprompt --encrypted --no-adduser
--no-createdb username`
-       Create the database
This step is only necessary if you don't already have a database setup (e.g. by your host) or you want to create new database for use with Drupal only. 

####sites/default/default.settings.php
- duplicate and save as settings.php

####Run install

####Download and add Bootstrap theme

#### Download required modules for the theme

