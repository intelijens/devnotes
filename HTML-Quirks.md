####How to remove dotted outline on hrefs and buttons:

a:focus { 
    outline: none; 
}
button::-moz-focus-inner {
  border: 0;
}

####CSS trick: Always show scrollbars
html {
     overflow: -moz-scrollbars-vertical;
     overflow: scroll;
}


