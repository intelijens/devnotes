GIT-Notes
=========

Just a small list of things I'd like to remember regarding Git and GitHub

<h4>GETTING STARTED</h4>

DOWNLOAD file
INSTALL git-1.8.4.2-intel-universal-snow-leopard.dmg


> HAD PROBLEM WITH git-2.0.1-intel-universal-snow-leopard.dmg<br/>
> Illegal instruction: 4<br/>
> Segmentation fault: 11<br/>
> as others reported, I received a seg fault with 2.0.1. Installed 1.8.4.2 from the legacy site: 

SET USERNAME<br/>
$ `git config --global user.name "johndoe"`

SET EMAIL<br/>
$ `git config --global user.email email@gmail.com`

CHECK YOUR SETTINGS <br/>
$ `git config --list`<br/>
$ `git config {key}`<br/>
> $ git config user.name<br/>
    	username

    
GET HELP <br/>   
$ `git help <verb>`<br/>
$ `git <verb> --help`<br/>
$ `man git-<verb>`

<h4>COMMANDS</h4>




$ `git clone git@github:me/name.git`<br/>
clones whole git repo including directory folder into current directory


$ `git clone git@github:me/name.git .`
clones only content into into current directory



$ `git remote -v`<br/>
go to directory of repo's cloned location

$ `git remote add upstream https://github.com/NAME/firstrepo.git`<br/>
Now, you can keep your fork synced with the upstream repository


$ `git checkout master`<br/>
Switched to branch 'master'

$ `git merge upstream/master`<br/>
Merge the changes from upstream/master into your local master branch. This brings your fork's master branch into sync with the upstream repository, without losing your local changes

$ `git status`<br/>

$ `git push`<br/>
from local to MASTER on GITHUB

$ `git pull`<br/>
from MASTER on GITHUB to local

$ `git branch`<br/>
lists branches

$ `git branch development`<br/>
creates new branch

$ `git checkout development`<br/>
switch to your branch
once in ur branch




###SSH keys
https://help.github.com/articles/generating-ssh-keys

Check for SSH keys
$ `ls -al ~/.ssh`<br/>
Lists the files in your .ssh directory, if they exist


Generate a new SSH key
$ `ssh-keygen -t rsa -C "your_email@example.com"`<br/>
Creates a new ssh key, using the provided email as a label
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/you/.ssh/id_rsa): [Press enter]

Enter passphrase (empty for no passphrase): [Type a passphrase]
Enter same passphrase again: [Type passphrase again]

Your identification has been saved in /Users/you/.ssh/id_rsa.
Your public key has been saved in /Users/you/.ssh/id_rsa.pub.
The key fingerprint is:
01:0f:f4:3b:ca:85:d6:17:a1:7d:f0:68:9d:f0:a2:db your_email@example.com

start the ssh-agent in the background
$ `eval "$(ssh-agent -s)"`<br/>
Agent pid 59566
$ `ssh-add ~/.ssh/id_rsa`

	
    
    
Your identification has been saved in /Users/NAME/.ssh/id_rsa.
Your public key has been saved in /Users/NAME/.ssh/id_rsa.pub.
The key fingerprint is:

11:7d:xx:56:11:6c:11:7a:x1:6a:1x:0f:xx:9a:11:bf EMAIL@gmail.com
The key's randomart image is:
+--[ RSA 2048]----+
|   o+            |
|   .   . +..     |
|      . o . *.o  |
|       . o =oo   |
|        S  .+o   |
|           .o .  |
|             +   |
|        o.  . .  |
|       oooEo     |
+-----------------+

####Add your SSH key to your account

$ `pbcopy < ~/.ssh/id_rsa.pub`
	Copies the contents of the id_rsa.pub file to your clipboard
	//Alternatively, using your favorite text editor, you can open the public key file and copy the contents of the file manually.



///////////////////////////////////////////////////////////////

##START FRESH

in Terminal
cd into the REPOSITORIES folder you have chosen to hold all of your repos

$ git init FOLDERNAME

$ vim filename.txt
	//or just drag and drop into folder system
	
$ git status

$ git add filename.txt
	//does not commit but stages it to commit
	//suggest to have it participate in
	
$ git commit -m "first commit"


$ git remote add origin git@github.com:intelijens/proj1.git


$ git push -u origin master

///////////////

##NOW PUT IT ON LIVE SERVER

in Terminal
cd into the LIVE folder you have chosen to hold all of your live repos


$ git clone https://github.com/xxxxxxxxx
	//now it's live

$ git pull origin master
	// once you have made updates on local and github, you can pull updates to live server







##NOT YET SURE HOW BEST TO DO THESE STEPS ALL WITIN ECLIPSE, TO AVOID THE COMMAND LINE AND/OR GOING TO THE GITHUB BROWSER GUI








##revert and stuff


If you want to remove newly added contents and files which are already staged (so added to the index) then you use:

git reset --hard

If you want to remove also your latest commit (is the one with the message "blah") then better to use:

git reset --hard HEAD^

To remove the untracked files (so new files not yet added to the index) and folders use:

git clean --force -d

http://jonas.nitro.dk/git/quick-reference.html

LASTLY
forces github to reflect the revert as well
git push --force












git config --global user.name "Your Name"
git config --global user.email you@example.com

If the identity used for this commit is wrong, you can fix it with:
git commit --amend --author='Your Name <you@example.com>'




###diff for all commits and file differences<br/>
$ `git diff de7c1166 b85c47cf > diff.txt`

###diff for just names of files<br/>
$ `git diff -–name-only de7c1166 b85c47cf > diff.txt`

###show which commit is showing<br/>
$ `git show --oneline -s`